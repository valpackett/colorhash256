# colorhash256 [![crates.io](https://img.shields.io/crates/v/colorhash256.svg)](https://crates.io/crates/colorhash256) [![API Docs](https://img.shields.io/badge/api-docs-yellow.svg?style=flat)](https://docs.rs/colorhash256) [![unlicense](https://img.shields.io/badge/un-license-green.svg?style=flat)](https://unlicense.org)

A [Rust] library that's like [Chroma-Hash], but with ANSI terminal colors.

[Rust]: https://www.rust-lang.org
[Chroma-Hash]: https://github.com/mattt/Chroma-Hash

## Usage

```rust
extern crate colorhash256;

let ansi = colorhash256::hash_as_ansi(b"Correct Horse Battery Staple");
let rgb = colorhash256::hash_as_rgb(b"Correct Horse Battery Staple");
```

## License

This is free and unencumbered software released into the public domain.  
For more information, please refer to the `UNLICENSE` file or [unlicense.org](https://unlicense.org).
